import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicBoxComponent } from './public-box.component';

describe('PublicBoxComponent', () => {
  let component: PublicBoxComponent;
  let fixture: ComponentFixture<PublicBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
