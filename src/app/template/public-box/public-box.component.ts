import { Component } from '@angular/core';

@Component({
  selector: 'app-public-box',
  template: `
    <div class="login-box">
      <ng-content></ng-content>
    </div>
  `
})
export class PublicBoxComponent {}
