import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-public-box-body',
  template: `
  <div class="login-box-body">
    <p class="login-box-msg">{{ title | translate }}</p>
    <ng-content></ng-content>
  </div>
  `
})
export class PublicBoxBodyComponent {
  @Input() title: string;
}
