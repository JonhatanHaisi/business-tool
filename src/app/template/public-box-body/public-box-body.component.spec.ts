import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicBoxBodyComponent } from './public-box-body.component';

describe('PublicBoxBodyComponent', () => {
  let component: PublicBoxBodyComponent;
  let fixture: ComponentFixture<PublicBoxBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicBoxBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicBoxBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
