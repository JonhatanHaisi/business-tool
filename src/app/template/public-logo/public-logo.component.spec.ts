import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicLogoComponent } from './public-logo.component';

describe('PublicLogoComponent', () => {
  let component: PublicLogoComponent;
  let fixture: ComponentFixture<PublicLogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicLogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
