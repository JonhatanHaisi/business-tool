import { Component } from '@angular/core';

@Component({
  selector: 'app-public-logo',
  template: `
  <div class="public-logo">
    <a href="#"><b>Business</b> Tool</a>
  </div>
  `
})
export class PublicLogoComponent {}
