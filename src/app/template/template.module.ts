import { NgModule } from '@angular/core';
import { Http } from '@angular/http';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { translateLoaderFactory } from '../core/miscellaneous/transalate-loader-factory';
import { environment } from '../../environments/environment';

import { PublicLogoComponent } from 'app/template/public-logo/public-logo.component';
import { PublicBoxBodyComponent } from './public-box-body/public-box-body.component';
import { PublicBoxComponent } from './public-box/public-box.component';

@NgModule({
    imports: [
        TranslateModule.forRoot({ loader: { provide: TranslateLoader, useFactory: translateLoaderFactory, deps: [ Http ] } })
    ],
    declarations: [
        PublicLogoComponent,
        PublicBoxComponent,
        PublicBoxBodyComponent
    ],
    exports: [
        PublicLogoComponent,
        PublicBoxBodyComponent,
        PublicBoxComponent,
    ]
})
export class TemplateModule {}
