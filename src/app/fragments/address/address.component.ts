import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent {

  controls = { zip: new FormControl(), address: new FormControl(), number: new FormControl(),
               city: new FormControl(), neighborhood: new FormControl(), state: new FormControl(), country: new FormControl() };

  @Input() model: any;

  constructor() { }

}
