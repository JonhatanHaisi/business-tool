import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Http } from '@angular/http';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { translateLoaderFactory } from 'app/core/miscellaneous/transalate-loader-factory';

import { ComponentsModule } from 'app/components/components.module';

import { AddressComponent } from './address/address.component';

@NgModule({
  imports: [
    ComponentsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({ loader: { provide: TranslateLoader, useFactory: translateLoaderFactory, deps: [ Http ] } })
  ],
  declarations: [
    AddressComponent
  ],
  exports: [
    AddressComponent
  ]
})
export class FragmentsModule {}
