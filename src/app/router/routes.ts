import { Routes } from '@angular/router';

// PAGES ======================================================
import { DashboardComponent } from 'app/pages/dashboard/dashboard.component';
import { EnterpriseComponent } from 'app/enterprise/enterprise/enterprise.component';
import { LoginPageComponent } from 'app/pages/login-page/login-page.component';
import { MainPageComponent } from 'app/core/main-page/main-page.component';
import { PasswordRecoveryPageComponent } from 'app/pages/password-recovery-page/password-recovery-page.component';
import { RegisterPageComponent } from 'app/pages/register-page/register-page.component';

// GUARDS =====================================================
import { AuthGuard } from './auth.guard';

export const routes: Routes = [

    { path: 'login', component: LoginPageComponent },
    { path: 'register', component: RegisterPageComponent },
    { path: 'password-recovery', component: PasswordRecoveryPageComponent },
    { path: '', component: MainPageComponent, canActivate: [ AuthGuard ],
        children: [
            {
                path: '', component: DashboardComponent, canActivate: [ AuthGuard ],
                data: { title: 'routes.dashboard.title', subtitle: 'routes.dashboard.subtitle' }
            },
            {
                path: 'enterprise', component: EnterpriseComponent, canActivate: [ AuthGuard ],
                data: { title: 'routes.enterprise.title', subtitle: 'routes.enterprise.subtitle' }
            }
        ]
    }

];
