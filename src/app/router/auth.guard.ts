import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { AuthService } from 'app/services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private authService: AuthService
    ) {}

    canActivate(): Observable<boolean> {
        return this.authService.authenticated
                   .map(result => this.mapAuthenticated(result));
    }

    private mapAuthenticated(result: any): boolean {
        if (result) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }

}
