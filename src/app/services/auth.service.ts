import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';

import 'rxjs/add/operator/first';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase';

@Injectable()
export class AuthService {

  constructor(public auth: AngularFireAuth) {}

  create(email: string, password: string): firebase.Promise<any> {
    return this.auth.auth.createUserWithEmailAndPassword(email, password);
  }

  signIn(email: string, password: string): firebase.Promise<boolean> {
    return this.auth.auth.signInWithEmailAndPassword(email, password);
  }

  signOut(): firebase.Promise<void> {
    return this.auth.auth.signOut();
  }

  sendPasswordResetEmail(email: string): firebase.Promise<any> {
    return this.auth.auth.sendPasswordResetEmail(email);
  }

  get authenticated(): Observable<firebase.User> {
      return this.auth.authState;
  }

}
