import { Injectable } from '@angular/core';

import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';

@Injectable()
export class UserService {

  constructor(private angularFire: AngularFireDatabase) {}

  create(user: any, uid: string): firebase.Promise<void> {
    return this.angularFire.object(`/user/${uid}`).set(user);
  }

  get (uid: string): FirebaseObjectObservable<any> {
    return this.angularFire.object(`/user/${ uid }`);
  }

}
