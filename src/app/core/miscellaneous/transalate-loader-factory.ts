import { Http } from '@angular/http';

import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function translateLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http);
}
