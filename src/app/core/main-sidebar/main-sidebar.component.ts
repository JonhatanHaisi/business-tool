import { Component, OnInit } from '@angular/core';

import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-main-sidebar',
  templateUrl: './main-sidebar.component.html',
  styleUrls: ['./main-sidebar.component.scss']
})
export class MainSidebarComponent implements OnInit {

  user: any;

  constructor(private userService: UserService) {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  ngOnInit() {
  }

  get shortName(): string {
        let name = '';

        if (this.user) {
            if (this.user.fullName.indexOf(' ') <= 0) {
                name = this.user.fullName;
            } else {
                const splittedName = this.user.fullName.split(' ');
                name = splittedName[0] + ' ' + splittedName[splittedName.length - 1];
            }
        }

        return name;
    }

}
