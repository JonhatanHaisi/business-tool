import { Component, HostBinding } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-navbar-custom-menu',
    template: '<ng-content></ng-content>'
})
export class NavbarCustomMenuComponent {

    @HostBinding('class') classes = 'navbar-custom-menu';

}
