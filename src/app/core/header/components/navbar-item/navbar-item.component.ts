import { Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-navbar-item',
    styleUrls: ['./navbar-item.component.scss'],
    template: `
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa {{ icon }}"></i>
        <!--span class="label label-success">4</span -->
    </a>
    <ng-content></ng-content>
    `
})
export class NavbarItemComponent {

    @HostBinding('class') classes = 'dropdown messages-menu';

    @Input() icon: string;

}
