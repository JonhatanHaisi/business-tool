import { Component, OnInit } from '@angular/core';

declare const $: any;

@Component({
    selector: 'app-control-sidebar-toggle',
    styleUrls: [ './control-sidebar-toggle.component.scss' ],
    template: '<a href="#" data-toggle="control-sidebar" data-target="#control-sidebar"><i class="fa fa-gears"></i></a>'
})
export class ControlSideberToggleComponent implements OnInit {

    ngOnInit() {
        $.AdminLTE.controlSidebar.activate();
    }

 }
