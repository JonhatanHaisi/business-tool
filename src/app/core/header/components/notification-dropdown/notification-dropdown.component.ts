import { Component } from '@angular/core';

@Component({
    selector: 'app-notification-dropdown',
    styleUrls: [ './notification-dropdown.component.scss' ],
    template: `
    <ul class="menu">
        <li>
            <!-- a href="#">
                <i class="fa fa-users text-aqua"></i> 5 new members joined today
            </a -->
        </li>
    </ul>
    `
})
export class NotificationDropdownComponent { }
