import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-header-logo',
    template: `
    <a href="#" class="logo">
      <span class="logo-mini"><b>B</b>T</span>
      <span class="logo-lg"><b>Business</b> Tool</span>
    </a>
    `
})
export class HeaderLogoComponent {}
