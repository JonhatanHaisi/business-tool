import { Component, HostBinding } from '@angular/core';

@Component({
    selector: 'app-navbar',
    template: '<ng-content></ng-content>'
})
export class NavbarComponent {

    @HostBinding('class') classes = 'nav navbar-nav';

}
