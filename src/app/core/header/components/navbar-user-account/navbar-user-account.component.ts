import { Component, HostBinding } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase';

import { AuthService } from 'app/services/auth.service';
import { UserService } from 'app/services/user.service';

@Component({
    selector: 'app-navbar-user-account',
    templateUrl: './navbar-user-account.component.html',
    styleUrls: [ './navbar-user-account.component.scss' ]
})
export class NavbarUserAccountComponent {

    @HostBinding('class') classes = 'dropdown user user-menu';

    user: any;

    constructor(
        private authService: AuthService,
        private userService: UserService,
        private router: Router
    ) {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.authService.authenticated
            .first()
            .subscribe(authState => this.checkAuthState(authState));
    }

    private checkAuthState(authState: firebase.User) {
        if (authState) {
            this.subscribeUser(authState.uid);
        }
    }

    private subscribeUser(uid: string): void {
        this.userService.get(uid).subscribe(user => {
            localStorage.setItem('user', JSON.stringify(user));
            this.user = user;
        });
    }

    signOut(): void {
        this.authService.signOut()
            .then(() => {
                this.router.navigate([ '/login' ]);
            });
    }

    get shortName(): string {
        let name = '';

        if (this.user) {
            if (this.user.fullName.indexOf(' ') <= 0) {
                name = this.user.fullName;
            } else {
                const splittedName = this.user.fullName.split(' ');
                name = splittedName[0] + ' ' + splittedName[splittedName.length - 1];
            }
        }

        return name;
    }

}
