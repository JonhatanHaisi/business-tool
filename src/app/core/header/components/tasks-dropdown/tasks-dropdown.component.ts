import { Component } from '@angular/core';

@Component({
    selector: 'app-tasks-dropdown',
    styleUrls: [ './tasks-dropdown.component.scss' ],
    template: `
    <ul class="menu">
        <li>
            <!--a href="#">
                <h3>
                    Design some buttons
                    <small class="pull-right">20%</small>
                </h3>
                <div class="progress xs">
                    <div class="progress-bar progress-bar-aqua" style="width: 20%"
                            role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                        <span class="sr-only">20% Complete</span>
                    </div>
                </div>
            </a -->
        </li>
    </ul>
    `
})
export class TasksDropdownComponent { }
