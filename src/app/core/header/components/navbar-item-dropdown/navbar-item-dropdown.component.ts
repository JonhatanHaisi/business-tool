import { Component, HostBinding, Input } from '@angular/core';

@Component({
    selector: 'app-navbar-item-dropdown',
    styleUrls: [ './navbar-item-dropdown.component.scss' ],
    template: `
    <ul>
        <li class="header">{{ header | translate }}</li>
        <li>
            <ng-content></ng-content>
        </li>
        <li class="footer"><a href="#">{{ footer | translate }}</a></li>
    </ul>
    `
})
export class NavbarItemDropdownComponent {

    @HostBinding('class') classes = 'dropdown-menu';

    @Input() header: string;
    @Input() footer: string;

}
