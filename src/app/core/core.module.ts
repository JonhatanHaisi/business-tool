import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Http } from '@angular/http';
import { RouterModule } from '@angular/router';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { translateLoaderFactory } from './miscellaneous/transalate-loader-factory';

import { InternalRouterModule } from 'app/router/internal-router.module';

import { environment } from '../../environments/environment';

import { BreadcrumbComponent } from 'app/core/content-wrapper/components/breadcrumb.component';
import { ContentWrapperComponent } from 'app/core/content-wrapper/content-wrapper.component';
import { ControlSideberToggleComponent } from 'app/core/header/components/control-sidebar-toggle/control-sidebar-toggle.component';
import { HeaderComponent } from 'app/core/header/header.component';
import { HeaderLogoComponent } from 'app/core/header/components/header-logo/header-logo.component';
import { MainSidebarToggleComponent } from 'app/core/header/components/main-sidebar-toggle/main-sidebar-toggle.component';
import { MessageDropdownComponent } from 'app/core/header/components/message-dropdown/message-dropdown.component';
import { NavbarComponent } from 'app/core/header/components/navbar/navbar.component';
import { NavbarCustomMenuComponent } from 'app/core/header/components/navbar-custom-menu/navbar-custom-menu.component';
import { NavbarItemComponent } from 'app/core/header/components/navbar-item/navbar-item.component';
import { NavbarItemDropdownComponent } from 'app/core/header/components/navbar-item-dropdown/navbar-item-dropdown.component';
import { NavbarUserAccountComponent } from 'app/core/header/components/navbar-user-account/navbar-user-account.component';
import { NotificationDropdownComponent } from 'app/core/header/components/notification-dropdown/notification-dropdown.component';
import { TasksDropdownComponent } from 'app/core/header/components/tasks-dropdown/tasks-dropdown.component';

@NgModule({
    imports: [
        BrowserModule,
        InternalRouterModule,
        TranslateModule.forRoot({ loader: { provide: TranslateLoader, useFactory: translateLoaderFactory, deps: [ Http ] } })
    ],
    declarations: [
        BreadcrumbComponent,
        ContentWrapperComponent,
        ControlSideberToggleComponent,
        HeaderComponent,
        HeaderLogoComponent,
        MainSidebarToggleComponent,
        MessageDropdownComponent,
        NavbarComponent,
        NavbarCustomMenuComponent,
        NavbarItemComponent,
        NavbarItemDropdownComponent,
        NavbarUserAccountComponent,
        NotificationDropdownComponent,
        TasksDropdownComponent
    ],
    exports: [
        ContentWrapperComponent,
        HeaderComponent
    ]
})
export class CoreModule {}
