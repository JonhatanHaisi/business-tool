import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, PRIMARY_OUTLET, Router } from '@angular/router';

@Component({
    selector: 'app-breadcrumb',
    template: `
    <section class="content-header">
        <h1>
            {{ lastItem.title | translate }}
            <small>{{ lastItem.subtitle | translate }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a routerLink="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li *ngFor="let page of breadcrumbs" [ngClass]="{ 'active': page.url === lastItem.url }">
                <a [routerLink]="[ page.url, page.params ]">{{ page.title | translate }}</a>
            </li>
            <!--li class="active">Blank page</li-->
        </ol>
    </section>
    `
})
export class BreadcrumbComponent {

    breadcrumbs: Array<any>;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        this.breadcrumbs = [];

        router.events.subscribe((event: NavigationEnd) => {
            if (event instanceof NavigationEnd) {
                this.breadcrumbs = this.buildBreadcrumb(this.activatedRoute.root);
            }
        });
    }

    private buildBreadcrumb(route: ActivatedRoute, url: string = '', breadcrumbs: Array<any> = []): any {
        const ROUTE_DATA_TITLE = 'title';
        const ROUTE_DATA_SUBTITLE = 'subtitle';

        const children: ActivatedRoute[] = route.children;

        for (const child of children) {
            if (child.outlet !== PRIMARY_OUTLET) {
                continue;
            }

            if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_TITLE)) {
                return this.buildBreadcrumb(child, url, breadcrumbs);
            }

            const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');
            url += `/${routeURL}`;

            breadcrumbs.push({
                title: child.snapshot.data[ROUTE_DATA_TITLE],
                subtitle: child.snapshot.data[ROUTE_DATA_SUBTITLE],
                url: url
            });

            return this.buildBreadcrumb(child, url, breadcrumbs);

        }

        return breadcrumbs;
    }

    get lastItem(): any {
        return this.breadcrumbs[this.breadcrumbs.length - 1];
    }

}
