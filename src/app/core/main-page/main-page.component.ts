import { Component, HostBinding, OnInit } from '@angular/core';

declare const $: any;

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  @HostBinding('class') classes = 'wrapper';

  constructor() { }

  ngOnInit() {
    $('body').attr({ class: 'fixed sidebar-mini skin-blue' });
  }

}
