import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Http } from '@angular/http';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { translateLoaderFactory } from 'app/core/miscellaneous/transalate-loader-factory';

import { MessageComponent } from 'app/components/form/message/message.component';
import { MessageDirective } from 'app/components/form/message/message.directive';

@NgModule({
    imports: [
        BrowserModule,
        TranslateModule.forRoot({ loader: { provide: TranslateLoader, useFactory: translateLoaderFactory, deps: [ Http ] } })
    ],
    declarations: [
        MessageComponent,
        MessageDirective
    ],
    exports: [
        MessageComponent,
        MessageDirective
    ],
    entryComponents: [
        MessageComponent
    ]
})
export class ComponentsModule {}
