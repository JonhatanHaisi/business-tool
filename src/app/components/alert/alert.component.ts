import { Component, ComponentFactoryResolver, ReflectiveInjector, ViewContainerRef, ViewChild } from '@angular/core';

import { AlertDangerComponent } from 'app/components/alert/alert-danger.component';
import { AlertSuccessComponent } from 'app/components/alert/alert-success.component';
import { AlertTypeEnum } from 'app/components/alert/alert-type.enum';

declare const $: any;

@Component({
  selector: 'app-alert',
  template: '<div #alertContainer></div>',
  entryComponents: [ AlertDangerComponent, AlertSuccessComponent ]
})
export class AlertComponent {

  currentComponent: any = null;

  @ViewChild('alertContainer', { read: ViewContainerRef }) alertContainer: ViewContainerRef;

  constructor(
    private resolver: ComponentFactoryResolver
  ) {}

  public showAlert(type: AlertTypeEnum, message = '') {
    const inputs = ReflectiveInjector.resolve([ { provide: 'message', useValue: message } ]);
    const injector = ReflectiveInjector.fromResolvedProviders(inputs, this.alertContainer.parentInjector);
    const factory = this.resolver.resolveComponentFactory(this.getAlertComponent(type));
    const component = factory.create(injector);
    this.alertContainer.insert(component.hostView);

    if (this.currentComponent) {
      this.currentComponent.destroy();
    }
    this.currentComponent = component;
  }

  private getAlertComponent(type: AlertTypeEnum): any {
    switch (type) {
      case AlertTypeEnum.SUCCESS:
        return AlertSuccessComponent;

      case AlertTypeEnum.DANGER:
        return AlertDangerComponent;
    }
  }

}
