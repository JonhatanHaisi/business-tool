import { Component, Injector, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    'selector': 'app-alert-danger',
    'template': `
    <div alert-danger alert-dismissible>
      <button alert-dismiss-button>×</button>
      {{ message | translate }}
    </div>
    `
})
export class AlertDangerComponent {

    @Input() message: string;

    constructor(injector: Injector) {
        this.message = injector.get('message');
    }

}
