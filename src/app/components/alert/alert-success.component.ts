import { Component, Injector, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    'selector': 'app-alert-success',
    'template': `
    <div alert-success alert-dismissible>
      <button alert-dismiss-button>×</button>
      {{ message | translate }}
    </div>
    `
})
export class AlertSuccessComponent {

    @Input() message: string;

    constructor(injector: Injector) {
        this.message = injector.get('message');
    }

}
