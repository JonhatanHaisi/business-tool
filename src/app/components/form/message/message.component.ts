import { Component, Injector } from '@angular/core';

import { MessagesMap } from 'app/components/form/message/messages-map';

@Component({
    selector: 'app-message',
    template: `
    <span *ngIf="component.invalid && (component.dirty || component.touched)">
      <span class="message-item" *ngFor="let message of messages">
        {{ messageMap[message] | translate }}
      </span>
    </span>
    `,
    styles: [
        `
        .message-item {
            color: red;
        }
        `
    ]
})
export class MessageComponent {

    messageMap = MessagesMap;

    private component: any;

    constructor(injector: Injector) {
        this.component = injector.get('component');
    }

    get messages(): Array<string> {
        const result = [];

        Object.keys(this.component.errors).forEach(key => {
            result.push(key);
        });

        return result;
    }

}
