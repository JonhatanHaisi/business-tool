export const MessagesMap = {
    'required': 'validation-messages.required',
    'minlength': 'validation-messages.minlength',
    'invalid-email': 'validation-messages.invalid-email'
}
