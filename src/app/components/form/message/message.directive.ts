import { ComponentFactoryResolver, Directive, Input, ReflectiveInjector, TemplateRef, ViewContainerRef } from '@angular/core';
import { MessageComponent } from 'app/components/form/message/message.component';

// tslint:disable:directive-selector
@Directive({ selector: '[message]' })
export class MessageDirective {

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainerRef: ViewContainerRef,
        private resolver: ComponentFactoryResolver
    ) { }

    @Input() set message(component: any) {
        const factory = this.resolver.resolveComponentFactory(MessageComponent);
        const injector = this.createInjector(component);

        this.viewContainerRef.createEmbeddedView(this.templateRef);
        this.viewContainerRef.createComponent(factory, undefined, injector);
    }

    private createInjector(component: any): any {
        const inputs = ReflectiveInjector.resolve([{ provide: 'component', useValue: component }]);
        return ReflectiveInjector.fromResolvedProviders(inputs, this.viewContainerRef.injector);
    }


}
