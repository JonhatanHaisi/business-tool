import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-input-message',
  templateUrl: './input-message.component.html',
  styleUrls: ['./input-message.component.scss']
})
export class InputMessageComponent {

  private readonly allMessages = {
    'required': 'validation-messages.required',
    'minlength': 'validation-messages.minlength',
    'invalid-email': 'validation-messages.invalid-email'
  };

  @Input() errors: any;

  constructor() {}

  get messages(): any[] {
    const result: Array<any> = [];

    if (this.errors) {
      for (const property in this.errors) {
        if (this.allMessages[property]) {
          const message = {
            text: this.allMessages[property],
            minLength: this.errors[property].requiredLength
          };
          result.push(message);
        }
      }
    }

    return result;
  }

}
