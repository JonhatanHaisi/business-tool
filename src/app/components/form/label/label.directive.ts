import { ComponentFactoryResolver, Directive, Input, ReflectiveInjector, TemplateRef, ViewContainerRef } from '@angular/core';

import { LabelComponent } from 'app/components/form/label/label.component';

// tslint:disable:directive-selector
@Directive({
  selector: '[label]'
})
export class LabelDirective {

  @Input() id: string;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) { }

  @Input() set label(label: string) {
    const factory = this.resolver.resolveComponentFactory(LabelComponent);
    const injector = this.createInjector(label);

    this.viewContainerRef.createComponent(factory, undefined, injector);
    this.viewContainerRef.createEmbeddedView(this.templateRef);
  }

  private createInjector(label: string): any {
    const inputs = ReflectiveInjector.resolve([ { provide: 'label', useValue: label }, { provide: 'for', useValue: this.id } ]);
    return ReflectiveInjector.fromResolvedProviders(inputs, this.viewContainerRef.injector);
  }

}
