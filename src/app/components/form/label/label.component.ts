import { Component, Injector, Input } from '@angular/core';

@Component({
  selector: 'app-label',
  template: '<label for="{{ for }}">{{ label }}</label>'
})
export class LabelComponent {

  @Input() for: string;
  @Input() label: string;

  constructor(injector: Injector) {
    this.label = injector.get('label') || this.label;
    this.for = injector.get('for') || this.for;
  }

}
