import { Component, Input } from '@angular/core';

// tslint:disable:no-input-rename
@Component({
  selector: 'app-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.scss']
})
export class InputContainerComponent {

  @Input() component: any;
  @Input('has-feedback') hasFeedback: boolean;

  constructor() { }

}
