import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { EnterpriseService } from 'app/enterprise/services/enterprise.service';

@Component({
  selector: 'app-enterprise',
  templateUrl: './enterprise.component.html',
  styleUrls: ['./enterprise.component.scss']
})
export class EnterpriseComponent implements OnInit {

  controls = { identifier: new FormControl(), officialName: new FormControl(), alias: new FormControl() };

  model = { address: {} };

  constructor(private enterpriseService: EnterpriseService) { }

  ngOnInit() {
  }

  doSubmit() {
    this.enterpriseService.create(this.model)
        .then(() => console.log('deu certo'));
  }

}
