import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';

@Injectable()
export class EnterpriseService {

    constructor(private angularFire: AngularFireDatabase) {}

    public create(enterprise: any): firebase.Promise<any> {
        return this.angularFire.object('/enterprise').set(enterprise);
    }

}
