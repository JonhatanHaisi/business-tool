import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { AlertComponent } from 'app/components/alert/alert.component';
import { AlertTypeEnum } from 'app/components/alert/alert-type.enum';
import { AuthService } from 'app/services/auth.service';
import { UserService } from 'app/services/user.service';

import { RegExpUtil } from 'app/util/regexp-util';

import * as firebase from 'firebase';

declare const $: any;

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  @ViewChild('alert') alert: AlertComponent;

  model: { fullName?: string, email?: string, password?: string } = {};

  email_regexp = RegExpUtil.EMAIL;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    $('body').attr({ class: 'hold-transition register-page' });
  }

  onSubmit(): void {
    this.authService.create(this.model.email, this.model.password)
        .then(authUser => this.createUser(authUser))
        .catch(() => this.alert.showAlert(AlertTypeEnum.DANGER, 'common.system-unavailable-try-again-later'));
  }

  private createUser(authUser: firebase.User): void {
    delete this.model.password;
    this.userService.create(this.model, authUser.uid)
        .then(() => this.router.navigate([ '/' ]))
        .catch(() => this.alert.showAlert(AlertTypeEnum.DANGER, 'common.system-unavailable-try-again-later'));
  }

}
