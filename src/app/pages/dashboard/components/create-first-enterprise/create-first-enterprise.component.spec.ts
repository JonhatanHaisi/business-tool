import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFirstEnterpriseComponent } from './create-first-enterprise.component';

describe('CreateFirstEnterpriseComponent', () => {
  let component: CreateFirstEnterpriseComponent;
  let fixture: ComponentFixture<CreateFirstEnterpriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFirstEnterpriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFirstEnterpriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
