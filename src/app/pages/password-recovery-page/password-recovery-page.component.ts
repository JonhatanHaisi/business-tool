import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { AlertComponent } from 'app/components/alert/alert.component';
import { AlertTypeEnum } from 'app/components/alert/alert-type.enum';
import { AuthService } from 'app/services/auth.service';

declare const $: any;

@Component({
  selector: 'app-password-recovery-page',
  templateUrl: './password-recovery-page.component.html',
  styleUrls: ['./password-recovery-page.component.scss']
})
export class PasswordRecoveryPageComponent implements OnInit {

  @ViewChild('alert') alert: AlertComponent;
  @ViewChild('passwordRecoveryEmail') emailComponent: AbstractControl;

  email: string;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    $('body').attr({ class: 'hold-transition login-page' });
  }

  onForgotPassword() {
    this.authService.sendPasswordResetEmail(this.email)
        .then(() => this.afterSendPaswordResetEmailSuccess())
        .catch(() => this.afterSendPaswordResetEmailFail());
  }

  private afterSendPaswordResetEmailSuccess() {
    this.emailComponent.reset();
    this.alert.showAlert(AlertTypeEnum.SUCCESS, 'password-recovery-page.recovery-email-sent')
  }

  private afterSendPaswordResetEmailFail() {
    this.alert.showAlert(AlertTypeEnum.DANGER, 'common.system-unavailable-try-again-later')
  }

}
