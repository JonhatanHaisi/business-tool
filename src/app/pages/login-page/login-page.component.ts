import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { AlertComponent } from 'app/components/alert/alert.component';
import { AlertTypeEnum } from 'app/components/alert/alert-type.enum';
import { AuthService } from 'app/services/auth.service';
import { RegExpUtil } from 'app/util/regexp-util';

import * as firebase from 'firebase';

declare const $: any;

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  @ViewChild('alert') alert: AlertComponent;

  model: { email?: string, password?: string } = {};

  email_regexp = RegExpUtil.EMAIL;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    $('body').attr({ class: 'hold-transition login-page' });
  }

  onSubmit(): void {
    this.authService.signIn(this.model.email, this.model.password)
        .then(authState => this.checkSignIn(authState))
        .catch(() => this.alert.showAlert(AlertTypeEnum.DANGER, 'login-page.invalid-email-or-password'));
  }

  private checkSignIn(authState: boolean): void {
    if (authState) {
      this.router.navigate([ '/' ]);
    }
  }

}
