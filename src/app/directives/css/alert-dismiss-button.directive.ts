import { Directive, HostBinding } from '@angular/core';

// tslint:disable:directive-selector
@Directive({
  selector: '[alert-dismiss-button]'
})
export class AlertDismissButtonDirective {

  @HostBinding('attr.type') useTypeButton = 'button';
  @HostBinding('class') useCloseClass = 'close';
  @HostBinding('attr.data-dismiss') useDataDimmissAlert = 'alert';
  @HostBinding('attr.aria-hidden') useAriaHidden = true;

  constructor() { }

}
