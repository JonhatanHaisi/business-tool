import { Directive, HostBinding } from '@angular/core';

// tslint:disable:directive-selector
@Directive({
  selector: '[alert-success]'
})
export class AlertSuccessDirective {

  @HostBinding('class') classes = 'alert alert-success';

  constructor() { }

}
