import { Directive, HostBinding } from '@angular/core';

// tslint:disable:directive-selector
@Directive({
  selector: '[row]'
})
export class RowDirective {

  @HostBinding('class') classes = 'row';

  constructor() { }

}
