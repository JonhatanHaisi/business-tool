import { Directive, HostBinding } from '@angular/core';

// tslint:disable:directive-selector
@Directive({
  selector: '[alert-danger]'
})
export class AlertDangerDirective {

  @HostBinding('class') classes = 'alert alert-danger';

  constructor() { }

}
