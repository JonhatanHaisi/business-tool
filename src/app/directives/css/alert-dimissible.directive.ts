import { Directive, HostBinding } from '@angular/core';

// tslint:disable:directive-selector
@Directive({
  selector: '[alert-dimissible]'
})
export class AlertDimissibleDirective {

  @HostBinding('class') classes = 'alert-dimissible';

  constructor() { }

}
