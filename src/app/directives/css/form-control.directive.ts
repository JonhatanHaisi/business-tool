import { Directive, HostBinding } from '@angular/core';

// tslint:disable:directive-selector
@Directive({
  selector: '[form-control]'
})
export class FormControlDirective {

  @HostBinding('class') classes = 'form-control';

  constructor() { }

}
