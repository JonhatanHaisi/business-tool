import { NgModule } from '@angular/core';

import { AlertDangerDirective } from './css/alert-danger.directive';
import { AlertDimissibleDirective } from './css/alert-dimissible.directive';
import { AlertDismissButtonDirective } from './css/alert-dismiss-button.directive';
import { EqualsDirective } from './validator/equals.directive';
import { EmailInputDirective } from './form/email-input.directive';
import { EmailValidatorDirective } from './validator/email-validator.directive';
import { ICheckboxDirective } from 'app/directives/form/i-checkbox.directive';
import { RowDirective } from 'app/directives/css/row.directive';
import { FormControlDirective } from './css/form-control.directive';
import { AlertSuccessDirective } from 'app/directives/css/alert-success.directive';


@NgModule({
    declarations: [
        AlertDangerDirective,
        AlertDimissibleDirective,
        AlertDismissButtonDirective,
        AlertSuccessDirective,
        EqualsDirective,
        EmailInputDirective,
        EmailValidatorDirective,
        FormControlDirective,
        ICheckboxDirective,
        RowDirective,
    ],
    exports: [
        AlertDangerDirective,
        AlertDimissibleDirective,
        AlertDismissButtonDirective,
        AlertSuccessDirective,
        EqualsDirective,
        EmailInputDirective,
        EmailValidatorDirective,
        FormControlDirective,
        ICheckboxDirective,
        RowDirective,
    ]
})
export class DirectivesModule {}
