import { ChangeDetectorRef, Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

// tslint:disable:directive-selector
@Directive({
  selector: '[equals]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => EqualsDirective), multi: true }
  ]
})
export class EqualsDirective implements Validator {

  constructor(
    @Attribute('equals') private validateEqual: string,
    @Attribute('reverse') private reverse: string
  ) {}

  private get isReverse() {
      if (!this.reverse) {
        return false;
      }
      return this.reverse === 'true' ? true : false;
  }

  validate(c: AbstractControl): { [key: string]: boolean } {
    const value = c.value === null ? undefined : c.value;
    const e = c.root.get(this.validateEqual);

    if (e && value === e.value) {
      this.removeError(e);
      this.removeError(c);
    }

    if (e && value !== e.value) {
      if (this.isReverse) {
        this.addError(e);

      } else {
        return { equals: true };

      }
    }

    return null;
  }

  private addError(control: AbstractControl): void {
    const errors = control.errors || {};
    errors['equals'] = true;
    control.setErrors(errors);
  }

  private removeError(control: AbstractControl): void {
    const errors = control.errors || {};
    delete errors['equals'];
    control.setErrors(Object.keys(errors).length ? errors : null);
  }

}
