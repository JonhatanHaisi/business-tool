import { Directive, forwardRef } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

import { RegExpUtil } from 'app/util/regexp-util';

// tslint:disable:directive-selector
@Directive({
  selector: '[email]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => EmailValidatorDirective), multi: true }
  ]
})
export class EmailValidatorDirective implements Validator {

  private emailPattern = RegExpUtil.EMAIL;

  constructor() { }

  validate(c: AbstractControl): { [key: string]: boolean } {

    const value = c.value;
    if (!this.emailPattern.test(value)) {
      return {
        'invalid-email': true
      };
    }

    return null;
  }

}
