import { Directive, HostBinding } from '@angular/core';

// tslint:disable:directive-selector
@Directive({
  selector: '[email]'
})
export class EmailInputDirective {

  @HostBinding('attr.type') type = 'email';
  @HostBinding('attr.spellcheck') spellcheck = 'false';

  constructor() { }

}
