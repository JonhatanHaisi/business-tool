import { Directive, ElementRef, HostBinding, OnInit } from '@angular/core';

declare const $: any;

// tslint:disable:directive-selector
@Directive({
  selector: '[i-checkbox]'
})
export class ICheckboxDirective implements OnInit {

  @HostBinding('class.checkbox') useCheckboxClass = true;
  @HostBinding('class.icheck') useICheckClass = true;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    $(this.el.nativeElement).find('input')
      .iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
      });
    }

}
