import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

// CORE IMPORTS =============================================================
import { AppComponent } from './app.component';
import { ControlSidebarComponent } from './core/control-sidebar/control-sidebar.component';
import { MainFooterComponent } from './core/main-footer/main-footer.component';
import { MainPageComponent } from './core/main-page/main-page.component';
import { MainSidebarComponent } from './core/main-sidebar/main-sidebar.component';

// PAGES ====================================================================
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { EnterpriseComponent } from './enterprise/enterprise/enterprise.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { PasswordRecoveryPageComponent } from './pages/password-recovery-page/password-recovery-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';

// MISCELLANEOUS =============================================================
import { translateLoaderFactory } from './core/miscellaneous/transalate-loader-factory';

// SERVICES ==================================================================
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

// INTERNAL MODULES ==========================================================
import { DirectivesModule } from 'app/directives/directives.module';
import { InternalRouterModule } from './router/internal-router.module';
import { InputMessageComponent } from './components/form/input-message/input-message.component';
import { InputContainerComponent } from './components/form/input-container/input-container.component';

// COMPONENTS ================================================================
import { AlertComponent } from './components/alert/alert.component';
import { AlertDangerComponent } from 'app/components/alert/alert-danger.component';
import { AlertSuccessComponent } from 'app/components/alert/alert-success.component';

// MODULES ===================================================================
import { ComponentsModule } from 'app/components/components.module';
import { CoreModule } from 'app/core/core.module';
import { FragmentsModule } from 'app/fragments/fragments.module';
import { TemplateModule } from 'app/template/template.module';

import { environment } from '../environments/environment';
import { CreateFirstEnterpriseComponent } from './pages/dashboard/components/create-first-enterprise/create-first-enterprise.component';
import { BoxComponent } from './components/box/box.component';
import { BoxHeaderComponent } from './components/box/box-header/box-header.component';
import { BoxBodyComponent } from './components/box/box-body/box-body.component';
import { BoxFooterComponent } from './components/box/box-footer/box-footer.component';
import { LabelDirective } from './components/form/label/label.directive';
import { LabelComponent } from './components/form/label/label.component';
import { EnterpriseService } from 'app/enterprise/services/enterprise.service';

@NgModule({
  declarations: [
    AppComponent,
    ControlSidebarComponent,
    DashboardComponent,
    EnterpriseComponent,
    LoginPageComponent,
    MainFooterComponent,
    MainPageComponent,
    MainSidebarComponent,
    RegisterPageComponent,
    InputMessageComponent,
    InputContainerComponent,
    AlertComponent,
    AlertDangerComponent,
    PasswordRecoveryPageComponent,
    AlertSuccessComponent,
    CreateFirstEnterpriseComponent,
    BoxComponent,
    BoxHeaderComponent,
    BoxBodyComponent,
    BoxFooterComponent,
    LabelDirective,
    LabelComponent,
  ],
  imports: [
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),
    BrowserModule,
    ComponentsModule,
    CoreModule,
    DirectivesModule,
    FormsModule,
    FragmentsModule,
    HttpModule,
    InternalRouterModule,
    ReactiveFormsModule,
    TemplateModule,
    TranslateModule.forRoot({ loader: { provide: TranslateLoader, useFactory: translateLoaderFactory, deps: [ Http ] } })
  ],
  providers: [
    AuthService,
    UserService,
    EnterpriseService
  ],
  entryComponents: [
    LabelComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
