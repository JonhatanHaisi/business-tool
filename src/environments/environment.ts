// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: 'AIzaSyBKmBHm4qKbxWBpYUcvxVhcL-w2eTQGNK0',
    authDomain: 'business-tool-dev.firebaseapp.com',
    databaseURL: 'https://business-tool-dev.firebaseio.com',
    projectId: 'business-tool-dev',
    storageBucket: 'business-tool-dev.appspot.com',
    messagingSenderId: '843262671087'
  }
};
