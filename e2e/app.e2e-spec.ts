import { BusinessToolPage } from './app.po';

describe('business-tool App', () => {
  let page: BusinessToolPage;

  beforeEach(() => {
    page = new BusinessToolPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
